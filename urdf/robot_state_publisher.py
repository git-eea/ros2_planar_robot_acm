#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Header

class RobotStatePublisher(Node):
    def __init__(self):
        super().__init__('robot_state_publisher')

        self.tf_broadcaster = self.create_timed_static_broadcaster()

    def create_timed_static_broadcaster(self):
        return self.create_timer(
            0.01,  # adjust the timer period as needed
            self.broadcast_static_transforms
        )

    def broadcast_static_transforms(self):
        tf_msg = TransformStamped()
        tf_msg.header = Header()
        tf_msg.header.stamp = self.get_clock().now().to_msg()
        tf_msg.header.frame_id = "odom"
        tf_msg.child_frame_id = "axis"
        tf_msg.transform.translation.x = 0.0
        tf_msg.transform.translation.y = 0.0
        tf_msg.transform.translation.z = 0.0
        tf_msg.transform.rotation.x = 0.0
        tf_msg.transform.rotation.y = 0.0
        tf_msg.transform.rotation.z = 0.0
        tf_msg.transform.rotation.w = 1.0

        self.static_broadcaster.sendTransform(tf_msg)


def main(args=None):
    rclpy.init(args=args)
    node = RobotStatePublisher()
    rclpy.spin(node)
    rclpy.shutdown()

if __name__ == '__main__':
    main()
