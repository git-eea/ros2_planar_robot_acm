#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "geometry_msgs/msg/twist.hpp"
#include "eigen3/Eigen/Dense"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "ros2_planar_robot/msg/joint_position.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

// Node publishing joint states that are used to visualize the robot in rviz
class JointPub : public rclcpp::Node
{
  public:
    JointPub()
    : Node("JointPub")
    {
        // Create publisher for /joint_states topic
        pub_joint_states_ = this->create_publisher<sensor_msgs::msg::JointState>(
            "/joint_states", 10);

        // Create subscriber for /q topic
        sub_q_ = this->create_subscription<ros2_planar_robot::msg::JointPosition>(
            "/q", 10, std::bind(&JointPub::qCallback, this, _1));
    }

  private:
    // Callback function for /q topic
    void qCallback(const ros2_planar_robot::msg::JointPosition::SharedPtr msg)
    {
        // Extract joint positions from the /q message
        double q0 = msg->q[0];
        double q1 = msg->q[1];

        // Create JointState message
        auto joint_state_msg = sensor_msgs::msg::JointState();
        rclcpp::Time now = this->get_clock()->now();
        joint_state_msg.header.stamp = now;
        joint_state_msg.name = {"r1", "r2"}; // Check joint names in the urdf file
        joint_state_msg.position = {q0, q1};

        // Publish the joint states on /joint_states topic
        pub_joint_states_->publish(joint_state_msg);
    }

    // Member variables
    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr pub_joint_states_;
    rclcpp::Subscription<ros2_planar_robot::msg::JointPosition>::SharedPtr sub_q_;
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<JointPub>());
  rclcpp::shutdown();
  return 0;
}