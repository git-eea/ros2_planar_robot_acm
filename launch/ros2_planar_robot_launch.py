import os
from launch_ros.substitutions import FindPackageShare
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution, TextSubstitution

def generate_launch_description():
    colors = {
        'background_r': '200'
    }

    return LaunchDescription([
        # Launch ros2_planar_robot nodes
        Node(
            package='ros2_planar_robot',
            executable='controller',
            name='controller'
        ),
        Node(
            package='ros2_planar_robot',
            executable='simulator',
            name='simulator'
        ),
        Node(
            package='ros2_planar_robot',
            executable='trajectory_generator',
            name='trajectory_generator'
        ),
        Node(
            package='ros2_planar_robot',
            executable='kinematic_modeller',
            name='kinematic_model',            
            parameters=[
                {"l1": 1.0},
                {"l2": 1.0},
            ]
        ),
        Node(
            package='ros2_planar_robot',
            executable='disturbance',
            name='disturbance'
        ),
        Node(
            package='ros2_planar_robot',
            executable='joint_pub',
            name='joint_pub'
        ),
        # Launch simple_visualizer.py nodes
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                PathJoinSubstitution([
                    FindPackageShare('ros2_planar_robot'),
                    'launch',
                    'simple_visualizer.py'
                ])
            )
        )
    ])